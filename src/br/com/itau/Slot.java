package br.com.itau;

import java.util.Objects;
import java.util.Random;

public class Slot<toSring> {
    private Opcoes opcao;

    public Slot() {
        Random random = new Random();
        int tamanhoDeOpcoes = Opcoes.values().length;
        int valorAleatorio = random.nextInt(tamanhoDeOpcoes);

        this.opcao = Opcoes.values()[valorAleatorio];
    }

    public Opcoes getOpcao() {

        return opcao;
    }

    @Override
    public String toString(){
        String modelo = "Slot " +
                "opcao = " + opcao + " - " +
                "pontos : " + opcao.pontos;
        return modelo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Slot<?> slot = (Slot<?>) o;
        return opcao == slot.opcao;
    }

    @Override
    public int hashCode() {
        return Objects.hash(opcao);
    }
}
