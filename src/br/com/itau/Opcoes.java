package br.com.itau;

import org.omg.CORBA.PUBLIC_MEMBER;

public enum Opcoes {
    BANANA(10),
    FRAMBOESA(50),
    MOEDA(100),
    SETE(300);

    public int pontos;

    Opcoes(int pontos){
        this.pontos = pontos;
    }
}
